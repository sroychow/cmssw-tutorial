import FWCore.ParameterSet.Config as cms

process = cms.Process("Test")

process.source = cms.Source("PoolSource",
  fileNames = cms.untracked.vstring(
      # Multiple file should be comma separated
      # This is the format for using a remote file
      'file:/eos/cms/store/mc/RunIII2024Summer24MiniAOD/DYto2Mu-2Jets_Bin-0J-MLL-50_TuneCP5_13p6TeV_amcatnloFXFX-pythia8/MINIAODSIM/140X_mcRun3_2024_realistic_v26-v2/2530002/a1d9b7a4-4e0e-49fe-80de-4e0e71cf707d.root',
  )
)

process.load("FWCore.MessageLogger.MessageLogger_cfi")
process.MessageLogger.cerr.FwkReport.reportEvery = 1000
process.maxEvents = cms.untracked.PSet( 
    input = cms.untracked.int32(10) 
)

#Check the cfipython folder in CMSSW_BASE
process.load("PhysicsTools/PairProducer/userleptonSelector_cfi")

process.MINIAODSIMoutput = cms.OutputModule("PoolOutputModule",
    compressionAlgorithm = cms.untracked.string('LZMA'),
    compressionLevel = cms.untracked.int32(4),
    fileName = cms.untracked.string('myOutputFile.root'),
    outputCommands = cms.untracked.vstring("keep *")
)
process.pairProduce_step = cms.Path(process.userleptonSelector)
process.MINIAODSIMoutput_step = cms.EndPath(process.MINIAODSIMoutput)
process.schedule = cms.Schedule(process.pairProduce_step, process.MINIAODSIMoutput_step)
