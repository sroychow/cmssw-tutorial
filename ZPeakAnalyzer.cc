// -*- C++ -*-
//
// Package:    PhysicsTools/ZPeakAnalyzer
// Class:      ZPeakAnalyzer
//
/**\class ZPeakAnalyzer ZPeakAnalyzer.cc PhysicsTools/ZPeakAnalyzer/plugins/ZPeakAnalyzer.cc

 Description: [one line class summary]

 Implementation:
     [Notes on implementation]
*/
//
// Original Author:  Suvankar Roy Chowdhury
//         Created:  Wed, 04 Oct 2023 07:46:03 GMT
//
//

// system include files
#include <memory>

// user include files
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/one/EDAnalyzer.h"

#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"

#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "FWCore/Utilities/interface/InputTag.h"

#include "FWCore/ServiceRegistry/interface/Service.h"
#include "CommonTools/UtilAlgos/interface/TFileService.h"

#include "DataFormats/PatCandidates/interface/Electron.h"
#include "DataFormats/PatCandidates/interface/Muon.h"
//
// class declaration
//

class ZPeakAnalyzer : public edm::one::EDAnalyzer<edm::one::SharedResources> {
public:
  explicit ZPeakAnalyzer(const edm::ParameterSet&);
  ~ZPeakAnalyzer() override;

  //static void fillDescriptions(edm::ConfigurationDescriptions& descriptions);

private:
  void beginJob() override;
  void analyze(const edm::Event&, const edm::EventSetup&) override;
  void endJob() override;

  // ----------member data ---------------------------
  std::map<std::string,TH1F*> histContainer_; 
  // ----------member data ---------------------------     
  edm::EDGetTokenT<pat::MuonCollection> muonCollToken;
  edm::EDGetTokenT<pat::ElectronCollection> elecCollToken;
  // input tags 
  edm::InputTag muonSrc_;
  edm::InputTag elecSrc_;
};

//
// constants, enums and typedefs
//

//
// static data member definitions
//

//
// constructors and destructor
//
ZPeakAnalyzer::ZPeakAnalyzer(const edm::ParameterSet& iConfig) :
  histContainer_(),
  muonSrc_(iConfig.getUntrackedParameter<edm::InputTag>("muonSrc")),
  elecSrc_(iConfig.getUntrackedParameter<edm::InputTag>("elecSrc")) {

  muonCollToken = consumes<pat::MuonCollection>(muonSrc_);
  elecCollToken = consumes<pat::ElectronCollection>(elecSrc_);

}

ZPeakAnalyzer::~ZPeakAnalyzer() {
  // do anything here that needs to be done at desctruction time
  // (e.g. close files, deallocate resources etc.)
  //
  // please remove this method altogether if it would be left empty
}

//
// member functions
//

// ------------ method called for each event  ------------
void ZPeakAnalyzer::analyze(const edm::Event& iEvent, const edm::EventSetup& iSetup) {
  using namespace edm;

  // get pat muon collection 
  edm::Handle< std::vector<pat::Muon>> muons;
  iEvent.getByToken(muonCollToken, muons);

  // fill pat muon histograms
  for (auto it = muons->cbegin(); it != muons->cend(); ++it) {
    histContainer_["muonPt"] ->Fill(it->pt());
    histContainer_["muonEta"]->Fill(it->eta());
    histContainer_["muonPhi"]->Fill(it->phi());

    if( it->pt()>20 && fabs(it->eta())<2.1 ){
      for (auto it2 = muons->cbegin(); it2 != muons->cend(); ++it2){
	if (it2 > it){
	  // check only muon pairs of unequal charge 
	  if( it->charge()*it2->charge()<0){
	    if( it2->pt()>20 && fabs(it2->eta())<2.1 ){
	      histContainer_["mumuMass"]->Fill((it->p4()+it2->p4()).mass());
	    }
	  }
	}
      }
    }
  }

  // get pat electron collection 
  edm::Handle< std::vector<pat::Electron>> electrons;
  iEvent.getByToken(elecCollToken, electrons);

  // loop over electrons
  for (auto it = electrons->cbegin(); it != electrons->cend(); ++it) {
    histContainer_["elePt"] ->Fill(it->pt());
    histContainer_["eleEta"]->Fill(it->eta());
    histContainer_["elePhi"]->Fill(it->phi());    
  }

  // Multiplicity
  histContainer_["eleMult" ]->Fill(electrons->size());
  histContainer_["muonMult"]->Fill(muons->size() );

}

// ------------ method called once each job just before starting event loop  ------------
void ZPeakAnalyzer::beginJob() {
  // register to the TFileService
  edm::Service<TFileService> fs;


  histContainer_["mumuMass"]=fs->make<TH1F>("mumuMass", "mass",    90,   30., 120.);
  
  // book histograms for Multiplicity:

  histContainer_["eleMult"]=fs->make<TH1F>("eleMult",   "electron multiplicity", 100, 0,  50);
  histContainer_["muonMult"]=fs->make<TH1F>("muonMult",   "muon multiplicity",     100, 0,  50);

  // book histograms for Pt:

  histContainer_["elePt"]=fs->make<TH1F>("elePt",   "electron Pt", 100, 0,  200);
  histContainer_["muonPt"]=fs->make<TH1F>("muonPt",   "muon Pt", 100, 0, 200);

  // book histograms for Eta:
  histContainer_["eleEta"]=fs->make<TH1F>("eleEta",   "electron Eta",100, -5,  5);
  histContainer_["muonEta"]=fs->make<TH1F>("muonEta",   "muon Eta",  100, -5,  5);


  // book histograms for Phi:
  histContainer_["elePhi"]=fs->make<TH1F>("elePhi",   "electron Phi", 100, -3.5, 3.5);
  histContainer_["muonPhi"]=fs->make<TH1F>("muonPhi",   "muon Phi",     100, -3.5, 3.5);

}

// ------------ method called once each job just after ending the event loop  ------------
void ZPeakAnalyzer::endJob() {
  // please remove this method if not needed
}

/*
COMMENT--> For now this is commented out. We shall show an example of using this
// ------------ method fills 'descriptions' with the allowed parameters for the module  ------------
void ZPeakAnalyzer::fillDescriptions(edm::ConfigurationDescriptions& descriptions) {
  //The following says we do not know what parameters are allowed so do no validation
  // Please change this to state exactly what you do use, even if it is no parameters
  edm::ParameterSetDescription desc;
  desc.setUnknown();
  descriptions.addDefault(desc);

}
*/
//define this as a plug-in
DEFINE_FWK_MODULE(ZPeakAnalyzer);
