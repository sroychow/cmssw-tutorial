# CMSSW on github
https://github.com/cms-sw/cmssw

## Docs
http://cms-sw.github.io/

# cmssw-tutorial
In this tutorial, you shall learn about basic concepts for CMSSW. You shall learn how to setup a  CMSSW area, inspect inputs, run reconstruction & creating MINI/NANOAOD files. You shall also learn how to write a basic analyzer on a MiniAOD file as input.


## Getting started

Log into y account. Then create the ```CMSSW``` area

```
source /cvmfs/cms.cern.ch/cmsset_default.sh
cmsrel CMSSW_14_0_18
cd CMSSW_14_0_18/src
cmsenv
git cms-init
```

## Exercise 1 : EDM command line tools
### edmDumpEventContent: 
Command help: ```edmDumpEventContent <FileName>```

Inspect RAW data file: ```/eos/cms/store/data/Run2024B/Muon0/RAW/v1/000/379/011/00000/e8229ced-2a49-455a-ad41-14de7827d940.root```

Inspect a MINIAOD file: ```/eos/cms/store/mc/RunIII2024Summer24MiniAODv6/DYJetsToLL_M-50_TuneCP5_13p6TeV-madgraphMLM-pythia8/MINIAODSIM/Pilot2024wmLHEGS_150X_mcRun3_2024_realistic_v1-v2/2540000/dde92106-bf14-4f61-b467-23cfe5f1f80d.root```
 

### edmConfigDump
Command help: ```edmConfigDump <FileName>```

## Exercise 2 : Setting up for your code
In practical usage, you may need to modify some code in CMSSW. All plugins exist in ```<Package>/<Subpackage>``` structure. There are some git commands customized to work with CMSSW.  


```
cd CMSSW_14_0_18/src
git cms-addpkg PhysicsTools/PatExamples
```

### Exercise 2a : Writing your first EDAnalyzer

```
cd CMSSW_14_0_18/src
cd PhysicsTools/
mkedanlzr ZPeakAnalyzer
```
This will create a dummy package for you. Let's inspect what's inside...
Now let's do some top-up and use the plugin here.

### Exercise 2b : Writing your first EDProducer

```
cd CMSSW_14_0_18/src
cd PhysicsTools/
mkedprod PairProducer
```
This will create a dummy package for you. Let's inspect what's inside...
Now let's do some top-up and use the LeptonSelector plugin from the gitlab repo.
Copy the contents of ```LeptonSelector.cc``` and ```BuildFile.xml``` from gitlab in the plugin area.
Copy the ```pairProducer_cfg.py``` in the test directory inside the test folder/

To Compile , go to the CMSSW src directory and run
```scram b```

After this,
- check the content of the input file
- execute the ```pairProducer_cfg.py``` config.
- check the content of output file

Next steps
- Implement a selection for electrons in the same plugin LeptonSelector and write to the output.
- Write an analyzer which will take as input these new user defined/selected electrons or muons and make opposite charge di-lepton pairs out of them. Plot the kinematic properties of the di-lepton system(including mass) and write histograms to a root file.

DataFormats of interest -
```https://github.com/cms-sw/cmssw/blob/master/DataFormats/PatCandidates/interface/Muon.h```

### Exercise 3: runTheMatrix

This is a powerful tool which will allow you to generate config files for all steps - starting from generation to reconstruction...In this exercise, we shall try to learn how to use this tool and run wfs related to MC/Data.
```runTheMatrix.py --help```