// -*- C++ -*-
//
// Package:    PhysicsTools/LeptonSelector
// Class:      LeptonSelector
//
/**\class LeptonSelector LeptonSelector.cc PhysicsTools/LeptonSelector/plugins/LeptonSelector.cc

 Description: A simple plugin to produce selected leptons

 Implementation:
     [Notes on implementation]
*/

// system include files
#include <memory>

// user include files
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/stream/EDProducer.h"

#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"

#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "FWCore/Utilities/interface/StreamID.h"

#include "DataFormats/PatCandidates/interface/Muon.h"

//
// class declaration
//

class LeptonSelector : public edm::stream::EDProducer<> {
public:
  explicit LeptonSelector(const edm::ParameterSet&);
  ~LeptonSelector() override;

  static void fillDescriptions(edm::ConfigurationDescriptions& descriptions);

private:
  void beginStream(edm::StreamID) override;
  void produce(edm::Event&, const edm::EventSetup&) override;
  void endStream() override;

  //void beginRun(edm::Run const&, edm::EventSetup const&) override;
  //void endRun(edm::Run const&, edm::EventSetup const&) override;
  //void beginLuminosityBlock(edm::LuminosityBlock const&, edm::EventSetup const&) override;
  //void endLuminosityBlock(edm::LuminosityBlock const&, edm::EventSetup const&) override;

  // ----------member data ---------------------------
    // ----------member data ---------------------------     
  edm::EDGetTokenT<pat::MuonCollection> muonCollToken;
  // input tags 
  edm::InputTag muonSrc_;

};

//
// constants, enums and typedefs
//

//
// static data member definitions
//

//
// constructors and destructor
//
LeptonSelector::LeptonSelector(const edm::ParameterSet& iConfig) :
  muonSrc_(iConfig.getParameter<edm::InputTag>("muonSrc")) {
  muonCollToken = consumes<pat::MuonCollection>(muonSrc_);

  //register your products
  //produces<ExampleData2>();

  //if do put with a label
  produces<std::vector<pat::Muon>>("userSelectedMuons");
 
  //now do what ever other initialization is needed
}

LeptonSelector::~LeptonSelector() {
  // do anything here that needs to be done at destruction time
  // (e.g. close files, deallocate resources etc.)
  //
  // please remove this method altogether if it would be left empty
}

//
// member functions
//

// ------------ method called to produce the data  ------------
void LeptonSelector::produce(edm::Event& iEvent, const edm::EventSetup& iSetup) {
  using namespace edm;

  // get pat muon collection 
  edm::Handle< std::vector<pat::Muon>> muons;
  iEvent.getByToken(muonCollToken, muons);

  std::vector<pat::Muon>  selmu;
  for(auto& imu: *muons) {
    if(imu.pt() < 15.) continue;
    selmu.emplace_back(imu);
  }
  
  //write into the Event
  iEvent.put(std::make_unique<std::vector<pat::Muon>>(std::move(selmu)), "userSelectedMuons");

}

// ------------ method called once each stream before processing any runs, lumis or events  ------------
void LeptonSelector::beginStream(edm::StreamID) {
  // please remove this method if not needed
}

// ------------ method called once each stream after processing all runs, lumis and events  ------------
void LeptonSelector::endStream() {
  // please remove this method if not needed
}

// ------------ method called when starting to processes a run  ------------
/*
void
LeptonSelector::beginRun(edm::Run const&, edm::EventSetup const&)
{
}
*/

// ------------ method called when ending the processing of a run  ------------
/*
void
LeptonSelector::endRun(edm::Run const&, edm::EventSetup const&)
{
}
*/

// ------------ method called when starting to processes a luminosity block  ------------
/*
void
LeptonSelector::beginLuminosityBlock(edm::LuminosityBlock const&, edm::EventSetup const&)
{
}
*/

// ------------ method called when ending the processing of a luminosity block  ------------
/*
void
LeptonSelector::endLuminosityBlock(edm::LuminosityBlock const&, edm::EventSetup const&)
{
}
*/

// ------------ method fills 'descriptions' with the allowed parameters for the module  ------------
void LeptonSelector::fillDescriptions(edm::ConfigurationDescriptions& descriptions) {
  //The following says we do not know what parameters are allowed so do no validation
  // Please change this to state exactly what you do use, even if it is no parameters
  edm::ParameterSetDescription desc;
  desc.setUnknown();
  desc.add<edm::InputTag>("muonSrc", edm::InputTag("slimmedMuons"));
  descriptions.add("userleptonSelector", desc);
}

//define this as a plug-in
DEFINE_FWK_MODULE(LeptonSelector);
